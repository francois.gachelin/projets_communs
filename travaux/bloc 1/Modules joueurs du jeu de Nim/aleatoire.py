import time

def definire_nom(truc):
    '''
    fait du nom une variable globale
    :return: None
    '''
    global nom_joueur
    nom_joueur=truc
    
def nom():
    return nom_joueur

def definire_id(nombre):
    '''
    fait de l id une variable globale
    :return: None
    '''
    global id_joueur
    id_joueur=nombre
    
def _id():
    return id_joueur

def joue(coups_possibles,situation,joueur_id):
    time.sleep(1)
    import random as rd
    coup=coups_possibles[rd.randint(0,len(coups_possibles)-1)]
    print(nom()," a tiré ",str(coup)," allumettes")
    return coup