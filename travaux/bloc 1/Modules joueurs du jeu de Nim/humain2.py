def definire_nom(truc):
    '''
    fait du nom une variable globale
    :return: None
    '''
    global nom_joueur
    nom_joueur=truc
    
def nom():
    return nom_joueur

def definire_id(nombre):
    '''
    fait de l id une variable globale
    :return: None
    '''
    global id_joueur
    id_joueur=nombre
    
def _id():
    return id_joueur

def joue(coups_possibles,situation,joueur_id):
    coup=int(input(nom()+", faites votre choix dans "+str(coups_possibles)[1:-1]+": "))
    return coup<4 and coup>1 and coup

