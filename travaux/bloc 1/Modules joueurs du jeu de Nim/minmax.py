import nim as jeu

import time

def definire_nom(truc):
    '''
    fait du nom une variable globale
    :return: None
    '''
    global nom_joueur
    nom_joueur=truc
    
def nom():
    return nom_joueur

def definire_id(nombre):
    '''
    fait de l id une variable globale
    :return: None
    '''
    global id_joueur
    id_joueur=nombre
    
def _id():
    '''
    :param: None
    :return: (int) valeur de id_joueur
    '''
    return id_joueur

def joue(coups_possibles,situation,joueur_id):
    '''
    Choisit un 'optimal' coup parmi les coups possibles 
    :param coups_possibles: (list)
    :param situation: (int) nombre d'allumettes
    :return: (int) valeur de id_joueur
    '''
    time.sleep(1)
    coup=minmax(situation,5,"PROGRAMME",1)[0]
    print(nom()," a tiré ",str(coup)," allumettes")
    return coup


def evaluer(situation, joueur_id):
    '''
    Estime la valeur de la situation, si elle est favorable pour gagner
    :param situation: (int) nombre d'allumettes
    :return: (int) valeur de la situation 
    '''
    if situation%5 == 3 or situation%5 == 2:
        valeur=1
    else:
        valeur=-1
    return valeur

def max_tuple(tab):
    '''
    Renvoie le tuple dont la composante2 est maximale
    :param tab: (list) liste de tuple d'entier
    :return: (tuple)
    '''
    indice_mx = 0
    mx = tab[0][1]
    for j in range(1,len(tab)):
        if tab[j][1] > mx:
            mx = tab[j][1]
            indice_mx = j
    return (indice_mx, mx)

def min_tuple(tab):
    '''
    Renvoie le tuple dont la composante2 est minimale
    :param tab: (list) liste de tuple d'entier
    :return: (tuple)
    '''
    indice_mn = 0
    mn = tab[0][1]
    for j in range(1,len(tab)):
        if tab[j][1] < mn:
            mn = tab[j][1]
            indice_mn = j
    return (indice_mn, mn)

def minmax(grille, profondeur, joueur, joueur_id):
    tab_coups_possibles = jeu.coups_possibles(grille)
    if jeu.jeu_fini(grille) or profondeur == 0:
        return (0, evaluer(grille, joueur_id))
    else:        
        if joueur == 'PROGRAMME':
            tab_grilles_suivantes = [ jeu.maj_situation_courante(grille, coup_possible, joueur_id) for coup_possible in tab_coups_possibles ]
            res = max_tuple( [ minmax(grillesuivante, profondeur - 1, 'ADVERSAIRE', joueur_id) for grillesuivante in tab_grilles_suivantes ] )
        else:
            tab_grilles_suivantes = [ jeu.maj_situation_courante(grille, coup_possible, joueur_id*(-1)) for coup_possible in tab_coups_possibles ]
            res = min_tuple( [ minmax(grillesuivante, profondeur - 1, 'PROGRAMME', joueur_id) for grillesuivante in tab_grilles_suivantes ] ) 
        return (tab_coups_possibles[res[0]],res[1])
    