# mini-jeux puissance 4
# Jean Goujon

from copy import deepcopy 

def situation_initiale():
    '''
    Renvoie un tableau a deux dimensions 6 lignes 7 colonnes
    :return: (list(list))
    '''
    return [ [0 for _ in range(7)] for _ in range(6)]

def affiche(grille):
    '''
    Affiche la grille dans la console
    :return: None
    :effet de bord: Affiche la grille dans la console
    '''
    for ligne in grille:
        sortie = ''
        for case in range(7):
            caractere = ' '
            if ligne[case] == 1:
                caractere = 'X'
            if ligne[case] == -1:
                caractere = 'O'
            sortie += '|'+caractere
            if case == 6:
                sortie += '|'
        print(sortie)
    print('|1|2|3|4|5|6|7|', end='\n\n')
            
def coups_possibles(grille):
    '''
    Renvoie un tableau contenant les colonnes que le joueur peut choisir 
    :return: (list)
    '''
    return [ i+1 for i in range(7) if grille[0][i] == 0 ]

def maj_situation_courante(grille, colonne, joueur_id):
    '''
    Renvoie la grille en applicant le coup choisi par le joueur_id 
    :return: (list)
    '''
    grille_dc = deepcopy(grille)
    if joueur_id == 1:
        nb = 1
    else:
        nb = -1
    hauteur = 0
    while grille_dc[5-hauteur][colonne-1] != 0:
        hauteur += 1
    grille_dc[5-hauteur][colonne-1] = nb
    return grille_dc
    
def jeu_fini(grille):
    '''
    Renvoie True si le jeu est fini, soit par victoire soit car plus de coup possible 
    :param: (list)
    :return: (bool)
    '''
    if not 0 in grille[0]:
        return True
    else:        
        for i in range(6): # test horizontal
            for j in range(4):
                if grille[i][j]+grille[i][j+1]+grille[i][j+2]+grille[i][j+3] in [4,-4]:
                    return True
        for i in range(3): # test vertical
            for j in range(7):
                if grille[i][j]+grille[i+1][j]+grille[i+2][j]+grille[i+3][j] in [4,-4]:
                    return True
        for i in range(3,6): # test diagonale haut
            for j in range(4):
                if grille[i][j]+grille[i-1][j+1]+grille[i-2][j+2]+grille[i-3][j+3] in [4,-4]:
                    return True
        for i in range(3): # test diagonale bas
            for j in range(4):
                if grille[i][j]+grille[i+1][j+1]+grille[i+2][j+2]+grille[i+3][j+3] in [4,-4]:
                    return True
        return False

def affiche_resultat(grille, joueur):
    if not 0 in grille[0]:
        print('Egalité')
    else:
        print('Joueur gagnant :', joueur.nom())
