def joue(coups_possibles,grille,joueur_id):
    '''
    Renvoie un élément du tableau des coups possibles
    :effet de bord: Affiche les propositions dans la console
    :return: (int)
    '''
    print(nom(),' Dans quelle colonne jouez-vous ?',coups_possibles)
    coup = int(input("Votre choix?"))
    
    try:
        not coup in coups_possibles    
    except AssertionError:
        print("Cette colonne n'est pas autorisée")
        joue(coups_possibles,grille)
    try:
        coup == ''   
    except AssertionError:
        print("Il faut choisir quelquechose")
        joue(coups_possibles,grille)  
        
    
    return coup

def definire_nom(truc):
    '''
    fait du nom une variable globale
    :return: None
    '''
    global nom_joueur
    nom_joueur=truc
    
def nom():
    return nom_joueur

def definire_id(nombre):
    '''
    fait de l id une variable globale
    :return: None
    '''
    global id_joueur
    id_joueur=nombre
    
def _id():
    return id_joueur
