import random as rd
import time

def joue(coups_possibles,grille,joueur_id):
    '''
    Renvoie un élément du tableau des coups possibles
    :effet de bord: Affiche les propositions dans la console
    :return: (int)
    '''
    coup = rd.choice(coups_possibles)
    print(nom(), ' joue colonne :', coup, end='\n\n')
    time.sleep(1)
    return coup

def definire_nom(truc):
    '''
    fait du nom une variable globale
    :return: None
    '''
    global nom_joueur
    nom_joueur=truc
    
def nom():
    return nom_joueur

def definire_id(nombre):
    '''
    fait de l id une variable globale
    :return: None
    '''
    global id_joueur
    id_joueur=nombre
    
def _id():
    return id_joueur
