## Jeux à deux joueurs, algorithme min-max

Le deuxième jeu est le puissance 4

Pour l'instant, les modes disponibles sont
* minmax vs humain
* minmax vs aleatoire
* humain vs humain
* humain vs aleatoire
* aleatoire vs aleatoire


-------

Remarques:

* minmax ne peut jouer qu'en premier

* Pour les autres types de joueur
    * humain
    * aleatoire

ont été créés deux modules, un par joueur pour chacun des deux jeux.

Il n'est en effet pas possible de créer deux 'instances' d'un même module.

La programmation orientée objet serait plus judicieuse ici.
