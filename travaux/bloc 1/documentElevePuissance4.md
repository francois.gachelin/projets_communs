# Les jeux à deux joueurs 

## Quelques exemples

Lire les notices des jeux (en annexe)

* Puissance 4
* Othello
* jeu de dames
* jeu d'échecs
* ...

**Q1.** Noter les points communs à toutes ces règles et décrire un scénario général commun à tous ces jeux.

## Les règles du jeu

Le déroulement de nombreux jeux videos à deux joueurs peut être décomposé de manière systématique.

Le tableau suivant indique la correspndance entre chaque étape du jeu et une implémentation possible en python.

Certaines fonction sont générales à tous les jeux, comme par exemple `choisir_joueur_initial(joueur1,joueur2)`.

D'autres sont spécifiques à chaque jeu et font partie d'un module spécifique appelé `jeu`.

|Scénario général|Exemple d'implémentation python|
|-----|-----|
|1. Qui va jouer?|joueur1,joueur2=definir_joueurs()|
|2. Situation initiale|situation=jeu.situation_initiale()|
|3. Choix du premier joueur|joueur_courant=choisir_joueur_initial(joueur1,joueur2)|
|4. Affichage de la situation initiale|jeu.affiche(situation)|
|5. Tant que le jeu n'est pas fini:|while not jeu.jeu_fini(situation):|
|&emsp;&emsp;1. le jeu propose les coups possibles|&emsp;&emsp;coups_possibles=jeu.coups_possibles(situation)|
|&emsp;&emsp;2. le jeu propose les coups possibles|&emsp;&emsp;coup=joueur_courant.joue(coups_possibles,situation)|
|&emsp;&emsp;3. Nouvelle situation de jeu|&emsp;&emsp;situation=jeu.maj_situation_courante(situation, coup, joueur_courant._id())|
|&emsp;&emsp;4. Affichage de la nouvelle situation|&emsp;&emsp;jeu.affiche(situation)  |
|&emsp;&emsp;5. C'est à l'autre joueur de jouer|&emsp;&emsp;joueur_courant=change_joueur_courant(joueur_courant,joueur1,joueur2)|
|6. Affichage de la situation finale|jeu.affiche_resultat(situation, joueur_courant)|


## Illustration des fonctions précedentes pour le jeu *puissance 4*

On s'intéresse au contenu du module `jeu.py` utilisé pour le jeu *puissance 4*.

**Q2.** Quel type de données choisiriez-vous pour décrire la situation initiale du jeu?
S'inspirer de l'allure de la grille du puissance 4.

>>>
On nomme `situation` cette variable.
>>>

**Q3.** Ecrire la fonction `situation_initiale()` qui créé la variable `situation`.

**Q4.** Ecrire la fonction `choisir_joueur_initial(joueur1,joueur2)` qui choisit aléatoiement l'un des deux joueurs.

```python
>>> choisir_joueur_initial('Grar','Krin')
Grar
```
ou
```python
>>> choisir_joueur_initial('Grar','Krin')
Krin
```

**Q5.** Ecrire la fonction `affiche(situation)` qui affiche dans le terminal l'allure 'réelle' de la grille de jeu de la manière suivante
```python
>>> situation_initiale()
>>> affiche(situation)
| | | | | | | |
| | | | | | | |
| | | | | | | |
| | | | | | | |
| | | | | | | |
| | | | | | | |
|1|2|3|4|5|6|7|
```

**Q6.** Quel type de variable retourne la fonction `jeu_fini(situation)`?
Expliquer comment cette fonction détermine si `situation` correspond ou non à la fin du jeu.

**Q7.** A quoi correspondent les coups possibles? En déduire un type possible de variable retournée par la fonction `coups_possibles(situation)`?
Quelle est la valeur de cette variable pour la situation initiale?

**Q8.** Ecrire la fonction `coups_possibles(situation)` qui a le comportement suivant

```python
>>> situation_initiale()
>>> cp=coups_possibles(situation)
>>> print(cp)
[1,2,3,4,5,6,7]
```

**Q9.** Ecrire la fonction `joue(coups_possibles,situation)` qui demande au joueur la colonne dans laquelle il va placer le pion, puis enregistre cette valeur dans la variable `choix_du_joueur`.
Elle devra avoir le comportement suivant

```python
>>> situation_initiale()
>>> cp=coups_possibles(situation)
>>> joue(cp,situation)
Dans quelle colonne jouez-vous? [1,2,3,4,5,6,7]
Votre choix: 
```

Après la saisie clavier `3` par exemple, la fonction indiquera ensuite la valeur enregistrée dans le terminal sous la forme
```python
choix_du_joueur=3
```

>>>
Dans la variable `situation`, les jetons du joueur 1 seront notés `X` et ceux du joueur 2 `O`.
>>>

**Q10.** Quelle est l'action de la fonction `maj_situation_courante(situation, coup, joueur_courant` sur la situation?
Pourquoi prend-elle `joueur_courant` en argument?
Pourquoi prend-elle `coup` en argument?

**Q11.** Ecrire la fonction `change_joueur_courant(joueur_courant,joueur1,joueur2)` qui change la valeur de `joueur_courant`.
```python
>>> joueur1="Bob"
>>> joueur2="Jo"
>>> joueur_courant=joueur1
>>> joueur_courant = change_joueur_courant(joueur_courant,joueur1,joueur2)
>>> joueur_courant
Jo
```

## Votre mission: implémentation d'un autre module `jeu`

**Q12.** Définir toutes les fonctions du module jeu dans le cas du *jeu de Nim* dans le fichier `jeu_de_Nim.py`.

**Q13.** Vérifier son bon fonctionnement en exécutant le fichier 'interface_jeu.py' placé dans le même repertoire que votre module `jeu_de_Nim.py`.

## Pour jouer contre l'ordinateur

On peut facilement simuler par une algorithme un joueur adverse qui jouerait chaque coup au hasard, c'est-à-dire sans tenir compte de la situation.

**Q14.** Compléter la fonction `joue_aléatoirement(coups_possibles,situation)` du fichier 'joueur_aleatoire.py' pour le jeu de Nim.

Le repos fait partie du travail: vous pouvez enfin jouer!