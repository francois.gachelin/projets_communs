#déroulement général d'un jeu à deux

print('Vous pouvez choisir minmax en joueur1 uniquement')
print('Vous pouvez choisir aleatoire en joueur1 ou 2. ')
print('Entrer votre prénom pour joueur humain\n')





import puissance4 as jeu
#import nim as jeu
import random as rd

def definir_joueurs():
    '''
    Créé joueur1 et joueur2 par saisie clavier: humain ou minMAX ou aleatoire
    :return: (tuple) joueur1,joueur2
    '''
    joueur1=input("Entrer le nom du joueur 1: ")
    joueur2=input("Entrer le nom du joueur 2: ")
    
    if joueur1=="minmax":
        import minmax as joueur1
        joueur1.definire_nom("minmax")
    elif joueur1=="aleatoire":
        import aleatoire as joueur1
        joueur1.definire_nom("aleatoire")
    else:
        nom=joueur1
        import humain as joueur1
        joueur1.definire_nom(nom)
    joueur1.definire_id(1)
    
    if joueur2=="minmax":
        import minmax2 as joueur2
        joueur2.definire_nom("minmax")
    elif joueur2=="aleatoire":
        import aleatoire2 as joueur2
        joueur2.definire_nom("aleatoire")
    else:
        nom=joueur2
        import humain2 as joueur2
        joueur2.definire_nom(nom)
    joueur2.definire_id(-1)
    
    return joueur1, joueur2

def change_joueur_courant(joueur_courant,joueur1,joueur2):
    '''
    Redéfinit la variable joueur_courant
    :param joueur1,2: noms des joueurs 1 et 2 
    :return: (string) nom du joueur
    '''
    if joueur_courant==joueur1:
        joueur_courant=joueur2
    else:
        joueur_courant=joueur1
    return joueur_courant

def choisir_joueur_initial(joueur1,joueur2):
    '''
    Definit joueur_courant avec joueur 1 ou joueur 2
    :return: (joueur) 
    '''
    #return rd.random()<0.5 and joueur1 or joueur2
    return joueur2


joueur1,joueur2=definir_joueurs()

situation=jeu.situation_initiale()

jeu.affiche(situation)

joueur_courant=choisir_joueur_initial(joueur1,joueur2)

while not jeu.jeu_fini(situation):
    
    joueur_courant=change_joueur_courant(joueur_courant,joueur1,joueur2)
    
    coups_possibles=jeu.coups_possibles(situation)
    
    coup=joueur_courant.joue(coups_possibles,situation,joueur_courant._id())
    
    situation=jeu.maj_situation_courante(situation, coup, joueur_courant._id())
    
    jeu.affiche(situation)   
   
jeu.affiche_resultat(situation, joueur_courant)
