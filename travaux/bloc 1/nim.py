def situation_initiale():
    '''
    Nombre initial d'allumettes
    :return: (int)
    '''
    return 20


def affiche(situation):
    '''
    Affiche la situation
    :param situation: (int) le nombre restant d'allumettes
    :return: None
    '''
    print("\n-------------------------------------------\n")
    print(situation>1 and "Il y a "+str(situation)+" allumettes" or "Il y a "+str(situation)+" allumette")
    
    
def jeu_fini(situation):
    '''
    Teste si la situation du jeu correspond à une situation terminale
    :return: (boolean)
    '''
    return not (situation>1 and True)


def coups_possibles(situation):
    '''
    :param situation: (int) nombre d'allumettes restantes
    :return: (list) liste des coups possibles
    '''
    if situation > 2:
        return [2,3]
    else:
        return [2]
    

def maj_situation_courante(situation,coup,id_joueur):
    '''
    Demande une action au joueur et enregistre son choix
    numero de colonne puissance 4 ou nombre de cailloux nim...
    :return: (int) nombre d'allumettes
    '''
    if situation==2 and coup==3:
        return situation
    else:
        situation=situation-coup
        return situation

def affiche_resultat(situation, joueur_courant):
    if situation==1:
        print("Joueurs ex aequo")
    if situation==0:
        print(joueur_courant.nom(), " gagne!")
