import puissance4

def joue(coups_possibles,grille,joueur_id):
    '''
    Renvoie un élément du tableau des coups possibles
    :param coups_possibles: (list)
    :param grille: (list)
    :param joueur_id: (int)
    :return: (int) la colonne à jouer
    '''   
    return minmax(grille, 5, 'PROGRAMME', 1)[0]

def minmax(grille, profondeur, joueur, joueur_id):
    '''
    Renvoie le meilleur coup à jouer
    :param profondeur: (int)
    :param grille: (list)
    :param joueur: (str) 'PROGRAMME' ou 'ADVERSAIRE'
    :param joueur_id: (int)
    :return: (int) la colonne à jouer
    '''   
    tab_coups_possibles = puissance4.coups_possibles(grille)
    if puissance4.jeu_fini(grille) or profondeur == 0:
        return (0, evaluer(grille, joueur_id))
    else:        
        if joueur == 'PROGRAMME':
            tab_grilles_suivantes = [ puissance4.maj_situation_courante(grille, coup_possible, joueur_id) for coup_possible in tab_coups_possibles ]
            res = max_tuple( [ minmax(grillesuivante, profondeur - 1, 'ADVERSAIRE', joueur_id) for grillesuivante in tab_grilles_suivantes ] )
        else:
            tab_grilles_suivantes = [ puissance4.maj_situation_courante(grille, coup_possible, joueur_id*(-1)) for coup_possible in tab_coups_possibles ]
            res = min_tuple( [ minmax(grillesuivante, profondeur - 1, 'PROGRAMME', joueur_id) for grillesuivante in tab_grilles_suivantes ] )       
        return (tab_coups_possibles[res[0]],res[1])
    
def max_tuple(tab):
    '''
    Renvoie le tuple dont la composante2 est maximale
    :param tab: (list) liste de tuple d'entier
    :return: (tuple)
    '''
    indice_mx = 0
    mx = tab[0][1]
    for j in range(1,len(tab)):
        if tab[j][1] > mx:
            mx = tab[j][1]
            indice_mx = j
    return (indice_mx, mx)

def min_tuple(tab):
    '''
    Renvoie le tuple dont la composante2 est minimale
    :param tab: (list) liste de tuple d'entier
    :return: (tuple)
    '''
    indice_mn = 0
    mn = tab[0][1]
    for j in range(1,len(tab)):
        if tab[j][1] < mn:
            mn = tab[j][1]
            indice_mn = j
    return (indice_mn, mn)

def evaluer(grille, joueur_id):
    '''
    Renvoie +1000 si 4 croix ou 0 ou -1000 si 4 rond
    :param grille: (list) liste de liste
    :param joueur_id: (int) 1 ou -1
    :return: (int)
    '''
    valeur = 0
    for i in range(6):
        for j in range(4):
            somme = grille[i][j]+grille[i][j+1]+grille[i][j+2]+grille[i][j+3]
            valeur += somme_vers_valeur(somme)
    for i in range(3):
        for j in range(7):
            somme = grille[i][j]+grille[i+1][j]+grille[i+2][j]+grille[i+3][j]
            valeur += somme_vers_valeur(somme)
    for i in range(3):
        for j in range(4):
            somme = grille[i][j]+grille[i+1][j+1]+grille[i+2][j+2]+grille[i+3][j+3]
            valeur += somme_vers_valeur(somme)
    for i in range(3,6):
        for j in range(4):
            somme = grille[i][j]+grille[i-1][j+1]+grille[i-2][j+2]+grille[i-3][j+3]
            valeur += somme_vers_valeur(somme)
    return valeur

def somme_vers_valeur(somme):
    '''
    Renvoie une valeur selon une somme passée en paramètre
    :param somme: (int)
    :return: (int)
    '''
    val = 0
    if somme == 4:
        val += 10000
    if somme == -4:
        val -= 10000
    if somme == 3:
        val += 100
    if somme == -3:
        val -= 100
    if somme == 2:
        val += 1
    if somme == -2:
        val -= 1
    return val

#def test():
#    grille = [[0, 0, 0, 0, 0, 0, 0],
#              [0, 0, 0, 0, 0, 0, 0],
#              [0, 0, 0, 0, 0, 0, 0],
#              [0, 0, 0, 0,-1, 0, 0],
#              [0, 0, 0, 0,-1, 0, 0],
#              [1, 1, 0, 0,-1, 0, 0]]
#    evalu = evaluer(grille, 1)
#    print(evalu)
#    #mn_tupl = min_tuple([(1,45),(8,54),(6,33)])
#    #print(mn_tupl)
#    mm = minmax(grille, 3, 'PROGRAMME', 1)
#    print(mm)
#    #tab_coups_possibles = puissance4.coups_possibles(grille)
#    #print(tab_coups_possibles)
#    #grille = puissance4.maj_situation_courante(grille, 1, 1)
#    #print(grille)
#    #tab_grilles_suivantes = [puissance4.maj_situation_courante(grille, coup_possible, 1) for coup_possible in tab_coups_possibles]
#    #print(tab_grilles_suivantes)

def definire_nom(truc):
    '''
    fait du nom une variable globale
    :return: None
    '''
    global nom_joueur
    nom_joueur=truc
    
def nom():
    return nom_joueur

def definire_id(nombre):
    '''
    fait de l id une variable globale
    :return: None
    '''
    global id_joueur
    id_joueur=nombre
    
def _id():
    return id_joueur

