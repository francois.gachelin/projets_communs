

On montre que leur coût est
quadratique dans le pire cas.


# Comment estimer la complexité d'un algorithme?

## Exemple des algorithmes de tris de listes

Pour remplir une même fonctionnalité, différents algorithmes sont possibles. 

Quels critères possibles de classification des algorithmes pour une même fonction remplie?

* nombre d'opérations mathématiques réalisées par le processeur
* nombre de comparaisons
* durée d'exécution de l'algorithme
* utilisation de la mémoire


## Quels critères?

Après rappel des différents algorithmes de tris possibles

* le tri par sélection
* le tri par insertion
* le tri fusion (hors-programme)
* le tri rapide (hors-programme)

Les élèves appliquent pour commencer les algorithmes de tri *à la main* (activité avec les poids de masses croissantes)
Puis on leur demande le critère le plus pertinent pour décrire la complexité de l'algorithme.

Dans le cas d'un algorithme informatique,un critère auquel l'utilisateur sera sensible est bien sûr la durée d'exécution de l'algorithme.
De plus, les autres critères sont parfois inaccessibles si le code du tri est inconnu, alors qu'un simple timer permet 
de mesurer une durée d'exécution.

A cette liste on peut ajouter la fonction de tri `sort()` qui fait partie du langage python et qui sera étudiée séparément.


## Descrition du protocole de comparaison

### Axes de réflexion

* Les durées d'exécution des programmes sont d'ordres de grandeurs négligeables devant la seconde, il est donc utile de répéter plusieurs fois le même calcul.
De plus cela permet d'établir une valeur moyenne d'exécution pour une tâche donnée.
* On peut se demander comment varie la durée d'exécution en fonction de la taille de la liste? On fera donc des séries de mesure pour plusieurs longueurs de listes.
* On peut aussi si la durée d'exécution varie en fonction de liste, si elle déjà triée ou totalement mélangée? On fera donc des séries de mesure avec des listes
triées et des listes mélangées.
* Une représentation graphique des résultats est pertinente pour facilement visualiser leur allure.

La fonction `comparatif` réalise les mesures puis génère leur représentation graphique.
Elle aura donc pour arguments
* `n_max`: la taille maximale des listes à trier (boucle de 1 à n_max)
* `liste`: triée ou aléatoire
* `liste_tris`: la liste des types de tri à analyser (boucle sur les 5 types de tri)

##  Les résultats

* Comparatif pour les listes triées croissantes

![listes triées croissantes](comparaison_4tris_cas_croissant.png)

* Comparatif pour les listes triées décroissantes

![listes triées décroissantes](comparaison_4tris_cas_decroissant.png)

* Comparatif pour les listes mélangées

![listes mélangées](comparaison_4tris_cas_melange.png)

### Axes de réflexion

* Compréhension de la représentation graphique. Les grandeurs et leurs unités en abscisse et en ordonnée.
* Notion de pire et de meilleur cas pour lesquels il y a une durée amximale ou minimale d'exécution
    * une liste quelconque sera triée en une durée intermédiaire
    * la liste triée croissante est-elle vraiment le meilleur cas?
    * le pire cas est-il la liste mélangée ou la liste triée décroissante?

On note
* `T` la durée d'exécution
* `n` le nombre d'éléments de la liste

* Allures des courbes
    * `T` proportionnelle à `n` pour certains algorithmes dans le meilleur des cas
    * `T` proportionnelle à `n²` pour certains algorithmes dans le pire des cas
    * pourquoi certains algorithmes ont toujours la même allure quelle que soit `n`?
    * Pouvait-on prévoir ces résultats en analysant les algorithmes?

* Peut-on dire de manière générale que tri ... est meilleur que tri ... de façon générale?
On poura compléter des tableaux avec des valeurs de `T` pour quelques valeurs de `n` dans différentes situations.
* Notion de complexité
    * en `n` 
    * en `n`&times;log(`n`) (fonction log vue en terminale)
    * en `n²` (le programme mentionne l'allure quadratique dans le pire des cas)



## Résultats avec la fonction `sort()` de python

* Comparatif pour les listes triées croissantes

![listes triées croissantes](croissante_sort.png)

* Comparatif pour les listes triées décroissantes

![listes triées décroissantes](decroissante_sort.png)

* Comparatif pour les listes mélangées

![listes mélangées](melangees_sort.png)

 
### Axes de réflexion

* De quel type de tri la fonction `sort()` se rapproche-t-elle le plus?
* Quelle est la particularité de ce tri pour les listes triées croissantes et les listes triées décroissantes?
* Commenter l'allure de `T`(`n`)
 