#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:auteur: François Gachelin et Jean Goujon
:date: Juin, 2019
"""

import TSP_biblio as t

def cree_distances(fichier):
    '''
    Creation du tableau des distances entre les villes du fichier
    :param fichier: (str) nom du fichier.txt avec villes et coordonnées
    :return: (list)(list) Tableau des distances entre les villes
    '''
    liste=t.get_tour_fichier(fichier)
    distances=[]
    fin=len(liste)
    for i in range(fin):
        distances.append([])
        for j in range(fin):
            distances[i].append(t.distance(liste,i,j))
            if i==j:
                distances[i][j]=1000000
    return distances
            
            
def ville_plus_proche(distances,indice_ville):
    '''
    Détermine la ville la plus proche d'une ville repérée par son indice
    :param distances: (list)(list)
    :param indice_ville: (int)
    :return: (int) indice de la ville la plus proche 
    '''
    return distances[indice_ville].index(min(distances[indice_ville]))

def efface_ville(distances,ville):
    '''
    Elimine une ville de la liste des villes possibles les plus proches
    en la plaçant très loin. Ligne et colonne correspondant à la ville
    ne sont pas réellement effacées du tableau distances
    :param ville: (int) indice de la ville à 'effacer du tableau'
    :return: (list)(list) Tableau distances mis à jour
    '''
    for i in range(len(distances[0])):
        for j in range(len(distances[0])):
            if i==ville:
                distances[i][j]=99999999999999999
            if j==ville:
                distances[i][j]=99999999999999999
    return distances   #très important de mettre un return pour modifier distances 'proprement'

def definir_parcours(ville_actuelle,liste,distances):
    '''
    Algorithme glouton qui parcours les villes en choisissant
    à chaque étape la ville suivante la plus proche
    :param ville: (int) indice de la ville à 'effacer du tableau'
    :return: (list)(list) Tableau distances mis à jour
    '''
    ville_arrivee=ville_actuelle
    parcours=[]
    n=len(distances[0])
    while n>0:
        ville_suivante=ville_plus_proche(distances,ville_actuelle)
        distances=efface_ville(distances,ville_actuelle)
        parcours.append(ville_actuelle)
        ville_actuelle=ville_suivante
        n=n-1
    parcours.append(ville_arrivee)
    tour=[]
    for i in parcours:
        tour.append(liste[i])
    return tour


fichier='exemple.txt'
liste=t.get_tour_fichier(fichier)
distances=cree_distances(fichier)
tour=definir_parcours(5,liste,distances)
t.trace(tour)
distances=cree_distances(fichier)