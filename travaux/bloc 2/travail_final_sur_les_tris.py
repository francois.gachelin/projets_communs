"""
Analyse en temps des algorithme de tris

:auteurs: Jean Goujon et François Gachelin
:date: Juin 2019
"""

from timeit import timeit
from matplotlib import pylab

def mesure(n):
    '''
    Renvoie le temps de calcul pour exécuter 100 tris de liste de taille n
    :param n: (int) taille de la liste
    :return: (float)
    '''
    return timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
       stmt='tri_select(cree_liste_melangee('+str(n)+'))',
       number=100)

def mesure_max(n):
    '''
    Affiche la courbe du temps d'execution de 100 tris selection en fonction de la taille de la liste
    :param n: (int) taille de la liste
    :return: None
    '''
    x = []
    y = []
    for i in range(1,n+1):
        x.append(i)
        y.append(timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
                          stmt='tri_select(cree_liste_melangee('+str(i)+'))',
                          number=100))
    courbe('tri select',x,y)
    
def courbe(titre,x,y):
    '''
    Affiche la courbe en prenant les données x en abscisse et y en ordonnée
    :param x: (list) liste des abscisses
    :param y: (list) liste des ordonnées
    :param titre: (str) taille de la liste
    :return: None
    '''
    NBRE_ESSAIS = 100
    pylab.title('Temps du '+titre+' (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.plot(x,y)
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.show()

def mesure_tri_max(tri,n):
    '''
    Affiche la courbe du temps d'execution de 100 tris passé en paramètre en fonction de la taille de la liste
    :param tri: (str) nom du tri
    :param n: (int) taille de la liste
    :return: None
    '''
    x = []
    y = []
    for i in range(1,n+1):
        x.append(i)
        y.append(timeit(setup='from tris import '+tri+'; from listes import cree_liste_croissante',
                          stmt=tri+'(cree_liste_croissante('+str(i)+'))',
                          number=100))
    courbe(tri,x,y)

def tous(n):
    '''
    Affiche les courbes des temps d'execution des tris : selection, insertion, rapide et fusion
    Pour 100 tris de liste de taille n    
    :param n: (int) taille de la liste
    :return: None
    '''
    tris = ['tri_select','tri_insert','tri_rapide','tri_fusion']
    x = []
    valeurs = [[],[],[],[]]
    for k in range(1,n+1):
        x.append(k)
    for i in range(4):       
        for j in range(1,n+1):
            valeurs[i].append(timeit(setup='from tris import '+tris[i]+'; from listes import cree_liste_decroissante',
                          stmt=tris[i]+'(cree_liste_decroissante('+str(j)+'))',
                          number=100))       
    pylab.plot(x,valeurs[0],label='select')
    pylab.plot(x,valeurs[1],label='insert')
    pylab.plot(x,valeurs[2],label='rapide')
    pylab.plot(x,valeurs[3],label='fusion')
    pylab.grid()
    pylab.legend()
    pylab.show()
