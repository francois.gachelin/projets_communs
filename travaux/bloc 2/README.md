# Analyse en temps des algorithme de tris

## Travaux réalisés

- Calculer le temps d'exécution de 100 tris (par selection) de liste de taille n
- Afficher la courbe du temps d'exécution de 100 tris (par selection) en fonction de la taille des listes
- Afficher la courbe du temps d'exécution de 100 tris (passé en paramètre) en fonction de la taille des listes
- Afficher sur un même graphique les courbes des quatre tris (selection, insertion, rapide, fusion) afin de les comparer
- Afficher ces même courbe en fonction du type de liste (triée croissante, triée décroissante, aléatoire)
- Fiche activité élève

# Voyageur de commerce

- Le code
- Une illustration de ce qui parait être optimum = sans intersection et en une boucle
![tourDeFrance](tour_de_France.png)