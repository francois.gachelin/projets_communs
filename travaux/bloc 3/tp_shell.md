# TP : Utiliser le Shell

---

Commencez par ouvrir l’invite de commande avec la touche window et en tapant cmd.

Dans la suite du TP, vous pouvez vérifier directement dans l’arborescence des dossiers le résultat de vos commandes.

1. Dans l'invite de commande window, afficher le chemin du répertoire courant avec la commande :

```
C:\Users>utilisateur>cd,
```

2. Remonter, plusieurs fois, dans l’arborescence jusqu’au disque **C:** avec la commande :

```
C:\Users>utilisateur>cd..
```

3. Créer un nouveau répertoire **paul** dans le répertoire courant **C:** avec la commande :

```
C:\mkdir paul
```

4. Visualiser le contenu du répertoire courant pour voir le répertoire **paul** avec la commande :

```
C:\dir
```

5. Se déplacer dans le répertoire **paul** avec la commande :

```
C:\cd paul
```

6. Remonter au niveau supérieur et créer un nouveau répertoire **pierre**. 

Les deux répertoires sont au même niveau.

7. Se placer dans le répertoire **pierre** et créer un nouveau fichier **comptebanque.txt** avec la commande :


```
C:\pierre\echo > comptebanque.txt
```

8. Ecrire du texte dans le fichier **comptebanque.txt** avec la commande :


```
C:\pierre\echo 2019 : 2000 euros > comptebanque.txt
```

9. Renommer le fichier **comptebanque.txt** en **banque.txt** avec la commande :


```
C:\pierre\rename comptebanque.txt banque.txt
```

10. Copier le fichier **banque.txt** dans le répertoire **paul** avec la commande :

```
C:\pierre\copy banque.txt C:\paul\
```

11. Se déplacer dans le répertoire **paul** et suprimer le fichier **banque.txt** :


```
C:\paul\del banque.txt
```

12. Remonter au niveau supérieur et suprimez le répertoire **paul** :

```
C:\rmdir paul
```

13. Se déplacer dans le répertoire **pierre** et créer le fichier **.secret**

Essayer de créer le fichier nommé **.secret2** dans l’interface classique windows.

14. Uniquement en ligne de commande, essayer de reproduire l’arbrescence suivante :

![](img/tp_shell-01.png "Arbrescence à reproduire")

