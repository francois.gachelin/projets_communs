# Lien symbolique - lien physique

## Objectifs
Après quelques rappels et précisions sur les raccourcis des environnements Windows, 
les élèves découvrent la notion de lien dans un environnement linux.
* connaitre la différence entre un lien symbolique et un lien physique
* créer un lien symbolique vers un fichier
* créer un lien physique vers un fichier
* comparer les différences de comportements vis à vis de la suppression
* dissocier l'accès aux données du stockage des données
* notion de persistance des données même si tous les liens sont supprimés

## Pré-requis
Environnement linux
* utilisation d'un terminal, commandes système
* système de fichiers en arborescence
* adresse physique des données

## Besoin techniques
* OS linux (installé ou sur virtualbox)

## Éléments de cours d'après le BO

Système d'exploitation
* Utiliser les commandes de base en ligne de commande.
* Les élèves utilisent un système d’exploitation libre.

## Séance pratique (document élève)

>>>
# Lien symbolique - lien physique

Pour ouvrir n'importe quel fichier ou répertoire depuis n'importe quel répertoire de l'arborescence,
il est possible de créer des accès à ces fichiers souvent qualifiés de *fichiers cibles*. 

Par exemple pour accéder au navigateur Firefox, il est plus facile de clicker sur l'icone `Firefox` du bureau
que d'accéder au fichier exécutable lui-même dans `C:\Program Files\Mozilla Firefox\firefox.exe`.

##Environnement Windows

Les accès sont appelés `raccourcis`. Ce type de fichier donne un accès direct à
sa cible via le système d'exploitation (mais n'est pas équivalent à ces données pour les autres logiciels).
On peut créer de nombreux raccourcis ou les supprimer sans influence sur leurs cibles.

##Environnement Linux

Les accès aux données sont appelés `liens`. (Contrairement aux raccourcis, ils sont dits `transparents` car ils sont équivalents à leurs cibles pour tous les logiciels.)

Il existe deux types de liens:
* les liens `physiques` qui pointent directement vers les données
* les liens `symboliques` qui pointent vers un lien physique

Par exemple ci-dessous une représentation des liens permettant d'accéder aux mêmes données depuis différents points du système de fichiers.
Il est important de noter la dissociation entre l'accès aux données et les données elles-mêmes stockées sur un disque dur par exemple.

![image](img/liensLinux.jpg)

**Q1.** Soulignez en vert les liens physiques et en bleu les liens symboliques

##Création de liens

La création de liens se fait dans un terminal avec la commande `ln` (abréviation de `link`) qui prend 
pour arguments le `nom_de_la_cible` et le `nom_du_lien`. Par exemple:

```python
>>> ln source.txt lien_1
```
créé un lien physique nommé `lien_1` qui pointe vers le fichier `source.txt`.

**Q2.** Créez un répertoire `test` dans `Documents`, créer un fichier `source.txt` dans lequel vous écrirez `Données physiques`.

Appliquez la ligne de commande de l'exemple ci-dessus. 

**Q3.** Effacez le fichier `source.txt` puis cliquez sur `lien_1`. Expliquez ce comportement.

**Q4.** Restaurez le fichier `source.txt` puis créez `lien_2` avec la commande suivante
```python
>>> ln -s source.txt lien_2
```
Effacez à nouveau le fichier `source.txt` puis cliquez sur `lien_2`. Expliquez ce comportement et en déduire la signification de 
l'argument `-s`.

**Q5.** Quelle différence fondamentale peut-on noter entre les liens symboliques et les liens physiques?

**Q6.** Restaurez le fichier `source.txt` puis créez `lien_3` avec la commande suivante

```python
>>> ln -s lien_2 lien_3
```
Quelle est la particularité de `lien_3`. Effacez `lien_2` puis cliquez sur `lien_3`. 
Expliquez ce comportement.

**Q7.** Ecrivez la ligne de commande qui créé un lien physique `lien_4` dans le repertoire `sousRepertoire` depuis un terminal ouvert dans le repertoire `test`.

**Q8.** Effacez tous le fichiers du repertoire `test` et vérifiez la persistance des données en cliquant sur `lien_4`.

>>>

## Exemples de QCM E3C

**Q.1** Quelle représentation est correcte?
1. ![reponse1](img/rep1.jpg) 
2. ![reponse2](img/rep2.jpg)
3. ![reponse3](img/rep3.jpg)
4. ![reponse4](img/rep4.jpg) |

**Q.2** Quelle est la proposition correcte?
1. Un lien symbolique pointe toujours vers un lien physique.
2. Un lien symbolique est un lien direct vers les données en memoire.
3. Un lien symbolique ne peut pas pointer vers un autre lien symbolique.
4. Un lien physique est un lien direct vers les données en mémoire.

**Q.3** Quelle commande correspond à la création d'un lien physique `lien_NSI` vers `NSI.txt` qui est dans le répertoire parent au repertoire courant ?
1. ln ../NSI.txt lien_NSI
2. ln ../NSI.txt lien_NSI
3. ln -s ../NSI.txt lien_NSI
4. ln -s ../NSI.txt lien_NSI

**Q.4** Quel est l'intérêt d'un lien physique?
1. Pour l'utilisateur, il fonctionne plus rapidement qu'un lien symbolique.
2. Il sécurise les données vis à vis des autres utilisateurs.
3. On peut en créer autant qu'on le souhaite.
4. Il reste valide même si le lien original est supprimé.


**Q.5** Quelle est la mauvaise proposition?
1. Les données contenues dans un fichier sont stockées dans le lien créé en même temps que le fichier.
2. Les données contenues dans un fichier sont stockées ailleurs que celles du lien créé en même temps que le fichier.
3. Les données contenues dans un fichier sont stockées sont accessibles par tous les liens.
4. Les liens symboliques pointent vers un lien physique.

**Q.6** Que se passe t-il si on supprime tous les liens physiques et matériels
1. Les données seront également supprimées de la mémoire.
2. Les données seront sur le disque jusqu'à ce que les 'cases mémoires' soit utilisées à autre chose.
3. Il est toujours possible de récupérer les données grâce à la corbeille.
4. Il sera impossible de retrouver ces données. 




