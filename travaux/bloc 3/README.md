# Activités élève autour des thémes du bloc3

## tp shell

Fiche tp sur les premières commandes shell sous windows dans le cas où l'accès
à linux n'est pas possible dans l'établissement.

- Descendre et monter dans l'arborescence
- Afficher le chemin du répertoire courant
- Voir le contenu d'un répertoire
- Créer, éditer, suprimer un fichier
- Créer, suprimer un répertoire
- Copier un fichier

### Exercices

- Reproduire une arborescence donnée


## Liens physiques, liens symboliques

- Objectifs
- Pré-requis
- Besoins techniques
- Éléments de cours d'après le BO

### Proposition de document élève

### Porposition de QCM E3C (6 questions)
