# Repertoire des documents évalués

## Lundi

* interface jeu à deux joueurs FAIT
* jeu humain vs humain FAIT 
* definition des noms et docstrings des fonctions des modules de type 'jeu' FAIT
* Jeu Nim implémenté FAIT
* Recherche des 'arbres gagnants' pour le jeu de Nim ...

## Mardi

* implémenter jeu Puissance4 FAIT
* poursuivre minMAX PRESQUE
* introduire un module joueur pour gérer humain vs minMAX FAIT
* Rédaction sujet élève mise en oeuvre du Puissance4 FAIT
* implémenter humain vs aleatoire FAIT
    
    
## Jeudi

* tests avec `sort` python FAIT
* activité élève comparaison tris PRESQUE
* copier puissance 4 et nim FAIT
* illustration QCM Q1. activité système sur les liens A FAIRE

## Tout envoyer A FAIRE !

